/*
 *
 *	File: HashTable.java
 *
 *	Authors: Ankit Gandhi <csghandiankit@gmail.com>
 *	         John Moon <johncarlmoon@gmail.com>
 *
 *	Purpose: Implements a simple hash table class.
 *
 */

public class HashTable
{
	int capacity;
	ListNode[] table;

	HashTable(int newCap)
	{
		capacity = newCap;
		table = new ListNode[capacity];	
		for (int i = 0; i < capacity; i++)
			table[i] = new ListNode();
	}

    public int getCap() {
        return capacity;
    }

    /* Increments the qty of given data. If data does not exist, creates data. */
    public void incCount(String data) {
		int index = hashCode(data);
		if (table[index].getCell() == null) { // Table index is empty
			table[index] = new ListNode(data);
		} else { // We  have a collision... let's traverse
			ListNode traverse = table[index];
			while (true) {
				if (traverse.getCell().data.equals(data)) { // There's a match, so increment qty
					traverse.incQty();
					break;
				} else {
					if (traverse.getNext() == null) { // No match, create new node and add to end
						traverse.setNext(new ListNode(data));
						break;
					} else {
						traverse = traverse.getNext(); // Move to the next node in list
					}
				}
			}
		}
	}

	/* Returns index based on modded sum of string's ascii values */
	public int hashCode(String data)
	{
		int length = data.length();
		int sum = 0;
		for (int i = 1; i < length; i++) {
			char ch = data.charAt(i);
			sum = sum + (int)ch;
		}
		return sum % getCap();
	}
}
