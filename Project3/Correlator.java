/*
 *
 *	File: Correlator.java
 *
 *	Authors: Ankit Gandhi <csghandiankit@gmail.com>
 *	         John Moon <johncarlmoon@gmail.com>
 *
 *	Purpose: To find a correlation number to represent the amount of
 *	         difference between two given texts.
 *
 */

import java.io.FileNotFoundException;
import java.io.IOException;

public class Correlator 
{
	/* Dissalows words with relative frequency f < 0.01% or f > 1.00% */
	public static final double MIN = 0.0001;
	public static final double MAX = 0.01;

	private static int count1 = 0;
	private static int count2 = 0;
	private static double total = 0.0;

	/* Processing command line args which define the backend counter */
	public static void main(String[] args) throws IOException 
	{
		if (args.length != 3) {
			System.out.println("Usage: [-b | -a | -h] <filename 1> <filename 2>\n");
			System.out.println("-b Use an unbalanced BST in the backend");
			System.out.println("-a Use an AVL Tree in the backend");
			System.out.println("-h Use a Hashtable in the backend");
		}

		DataCount<String>[] file1;
		DataCount<String>[] file2;

		file1 = correlator(args[0], args[1]);
		file2 = correlator(args[0], args[2]);

		for (DataCount<String> x : file1) {
			count1 += x.count;
		}
		for (DataCount<String> y : file2) {
			count2 += y.count;
		}

		correlator(file1, file2);
	}

	/* Helper function for finding the correlation */
	private static DataCount<String>[] correlator(String string, String string2)
												  throws FileNotFoundException,
												  IOException 
	{
		FileWordReader reader = new FileWordReader(string2);
		DataCounter<String> counter = null;

		if (string.equals("-b")) {
			counter = new BinarySearchTree<String>();
		} else if (string.equals("-a")) {
			counter = new AVLTree<String>();
		} else if (string.equals("-h")) {
			counter = new HashCounter();
		} else {
			return null;
		}

		String word = reader.nextWord();
		while (word != null) {
			counter.incCount(word);
			word = reader.nextWord();
		}

		return counter.getCounts();
	}

	/* Calculates the correlation between the two given texts */
	private static void correlator(DataCount<String>[] file1, DataCount<String>[] file2) 
	{
		for (DataCount<String> i : file1) {
			for (DataCount<String> j : file2) {
				if ((double) i.count / count1 < MAX && (double) i.count / count1 > MIN
						&& (double) j.count / count2 < MAX && (double) j.count / count2 > MIN) {
					if (i.data.equals(j.data)) {
						total += Math.pow((double) i.count / count1 - (double) j.count / count2, 2.0);
					}
				}
			}
		}
		System.out.println("Correlation: " + total);
	}

}
