/*
 *
 *	File: WordCount.java
 *
 *	Authors: Ankit Gandhi <csghandiankit@gmail.com>
 *	         John Moon <johncarlmoon@gmail.com>
 *
 *	Purpose: Performs word frequency analysis on the input file.
 *
 */

import java.io.IOException;
import java.util.Arrays;

public class WordCount 
{
	static boolean totals = true;
	static boolean frequency = false;
	static boolean unique = false;

    private static void countWords(String file, DataCounter counter) 
	{
		/* Process the file and add the data into the table or tree */
        try {
            FileWordReader reader = new FileWordReader(file);
            String word = reader.nextWord();
            while (word != null) {
                counter.incCount(word);
                word = reader.nextWord(); }
        } catch (IOException e) {
            System.err.println("Error processing " + file + e);
            System.exit(1);
        }

		/* Create an array of counts, then sort it */
        DataCount<String>[] counts = counter.getCounts();
        sortByDescendingCount(counts, 0, counts.length - 1);
		sortByAlpha(counts);

		/* Print the array after it's sorted */
		if (totals) {
        for (DataCount<String> c : counts)
            System.out.println(c.count + "  \t" + c.data);
		}

		/* Prints number of unique if specified */
		if (unique)
			System.out.println("Number of unique words: " + counts.length);

		/* Prints the array with overall frequency */
		if (frequency) {
			float numUnique = counts.length;
			for (DataCount<String> c : counts)
				System.out.println(c.count / numUnique + "  \t" + c.data);
		}
    }

	/* Sort the count array in descending order using the Quicksort algorithm */
    private static void sortByDescendingCount(DataCount<String>[] counts,
											  int lowIndex, int highIndex)
	{
		int i = lowIndex;
		int j = highIndex;
		int pivot =  counts[lowIndex + (highIndex - lowIndex) / 2].count; // Pivot is the middle of the array	

		while (i <= j) {
			while (counts[i].count > pivot) {
				if (i <= j)
					i++;
				else
					break;
			}
			while (counts[j].count < pivot) {
				if (j >= i)
					j--;
				else
					break;
			}

			if (i <= j) {
				DataCount<String> tmp = counts[i];
				counts[i] = counts[j];
				counts[j] = tmp;
				i++;
				j--;
			}
		}

		/* Recursively divide into subarrays */
		if (lowIndex < j)
			sortByDescendingCount(counts, lowIndex, j);
		if (i < highIndex)
			sortByDescendingCount(counts, i, highIndex);	
    }

	/* Sort array alphabetically as a secondary sort */	
	private static void sortByAlpha(DataCount<String>[] counts)
	{
		int arrSize = counts.length;
		int i = 0;
		while (i < arrSize) {	
			int tmpCount = counts[i].count; 
			int subArrLength;
			if (tmpCount > 1) {
				subArrLength = 1;
				for (int j = i + 1; counts[j].count == tmpCount; j++) {
					subArrLength++;
				}
			} else {
				subArrLength = arrSize - i;
			}

			if (subArrLength > 1)  {
				DataCount[] subArr = new DataCount[subArrLength];
				for (int x = 0; x < subArrLength; x++) {
					subArr[x] = counts[i + x];
				}

				Arrays.sort(subArr, new StringComparator());
				
				for (int x = 0; x < subArrLength; x++) {
					counts[i + x] = subArr[x];
				}
			}

			i = i + subArrLength;
		}
	}

	public static void main(String[] args) 
	{
        if (args.length < 2) {
            System.err.println("Error - Usage... \"WordCount [-a | -b | -h] [filename...] [-frequency] [-unique]\"");
            System.exit(1);
        }
		if (args.length > 2) {	
			if (args[2].compareTo("-frequency") == 0) {
				totals = false;
				frequency = true;
			}
			else if (args[2].compareTo("-unique") == 0) {
				totals = false;
				unique = true;
			} else { 
				System.err.println("Error - Usage... \"WordCount [-a | -b | -h] [filename...] [-frequency] [-unique]\"");
				System.exit(1);
			}

		}

	
		/* If we get the "-a" flag, use an AVL tree
		   If we get the "-h" flag, use a hash table */
		if (args[0].compareTo("-a") != 0)
			countWords(args[1], new AVLTree());
		else if (args[0].compareTo("-h") != 0)
			countWords(args[1], new HashCounter());
		else if (args[0].compareTo("-b") != 0)
			countWords(args[1], new BinarySearchTree());
		else {
			System.err.println("Error - Usage... \"WordCount [-a | -b | -h] [filename...] [-frequency] [-unique]\"");
		}

        System.exit(1);
    }
}
