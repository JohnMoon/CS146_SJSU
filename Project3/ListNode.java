/*
 *
 *	File: ListNode.java
 *
 *	Authors: Ankit Gandhi <csghandiankit@gmail.com>
 *	         John Moon <johncarlmoon@gmail.com>
 *
 *	Purpose: Simple node class for the linked lists in the HashTable.
 *
 */

public class ListNode
{
	private DataCount<String> cell;
	private ListNode next;

	ListNode()
	{
		cell = null;
		next = null;
	}

	ListNode(String newData)
	{
		cell = new DataCount<String>(newData, 1);
		next = null;
	}
	
	public void incQty()
	{
		cell.count = cell.count + 1;
	}

	public DataCount<String> getCell()
	{
		return cell;
	}

	public int getQty()
	{
		return cell.count;
	}

	public ListNode getNext()
	{
		return next;
	}

	public void setCell(DataCount<String> newCell)
	{
		cell = newCell;
	}

	public void setNext(ListNode newNext)
	{
		next = newNext;
	}
}
