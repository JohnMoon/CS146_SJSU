/*
 *
 *	File: StringComparator.java
 *
 *	Authors: Ankit Gandhi <csghandiankit@gmail.com>
 *	         John Moon <johncarlmoon@gmail.com>
 *
 *	Purpose: Quick class to make the DataCounts comprable for
 *	         alphabetic sorting.
 *
 */

import java.util.Comparator;

public class StringComparator implements Comparator<DataCount> {
	@Override
	public int compare(DataCount o1, DataCount o2) {
		String data1 = o1.data.toString();
		String data2 = o2.data.toString();
		return data1.compareTo(data2);
	}
}
