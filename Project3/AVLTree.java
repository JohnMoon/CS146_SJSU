/*
 *
 *	File: AVLTree.java
 *
 *	Authors: Ankit Gandhi <csghandiankit@gmail.com>
 *	         John Moon <johncarlmoon@gmail.com>
 *
 *	Purpose: Implements the backend AVLTree counter
 *
 */

public class AVLTree<E extends Comparable<? super E>> extends BinarySearchTree<E>implements DataCounter<E> 
{
	public AVLTree() 
	{
		super();
	}

	/* Adds data to tree, increments qty if data exists */
	public void incCount(E data) 
	{
		super.incCount(data);
		overallRoot = balance(overallRoot);
	}

	/* Balances the BST using the 4-case rotation model */
	private BSTNode balance(BSTNode node) 
	{
		if (node.left == null && node.right == null) {
			return node;
		}

		int comp = height(node.left) - height(node.right);
		if (comp > 1) {
			if (height(node.left.left) >= height(node.left.right)) {
				node = singleRightRotation(node);
			} else {
				node = doubleLeftRightRotation(node);
			}

		} else if (comp < 1) {
			if (height(node.right.right) >= height(node.right.left)) {
				node = singleLeftRotation(node);
			} else {
				node = doubleRightLeftRotation(node);
			}
		}
		return node;
	}

	/* Performs the rotation operations to balance the tree */
	private BSTNode singleLeftRotation(BSTNode k1) 
	{
		BSTNode right = k1.right;
		k1.right = right.left;
		right.left = k1;
		return right;
	}

	private BSTNode singleRightRotation(BSTNode k1) 
	{
		BSTNode left = k1.left;
		k1.left = left.right;
		left.right = k1;
		return left;
	}

	private BSTNode doubleRightLeftRotation(BSTNode k2) 
	{
		k2.right = singleRightRotation(k2.right);
		return singleLeftRotation(k2);
	}

	private BSTNode doubleLeftRightRotation(BSTNode k2) 
	{
		k2.left = singleLeftRotation(k2.left);
		return singleRightRotation(k2);
	}
}
