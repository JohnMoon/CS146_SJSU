1) We are group of two students – John Moon and Ankit Gandhi.

2) It was crazy journey which took over 40? hours.

3) We expected that HashTable to be fastest data structure since it has great efficiency at O(1) time.

4) The fastest data structure turned out to be the AVL tree for sorting and the HashTable for storing/calculating the correlation. We believe this is because for sorting, the AVL tree had the advantage in that the smallest count was always on top, making it an easy structure to sort into an array. The hash table ended up taking a good amount of time to go through each index/linked list to sort the data into an array.

5) In general, the AVL tree works well for the sorting portion of the assignment and it was relatively easy to code up (especially since the ADT was already developed in the last assignment). The hash table worked really well for the correlation though. It was irritating to code it up, but that had more to do with lack of familiarity with Java... it would have gone smoother in C.

6) The AVL tree was awful when it came to the correlation. Simply creating a tree full of the words in both files takes much longer with the AVL tree as it must continually rebalance the tree to maintain the AVL attributes. Since this had no sorting component, this step didn't really add anything to the end goal. The BST and hash table both accomplished to correlation in a much faster time because the initialy storage of the data was less of a hassel.

7) We believe Bacon _did_ write The New Atlantis. It had a much larger correlation (5.8) when compared to multiple Shakespeare works than other Shakespeare works (2.0 - 2.5).

8) See WriteUp.pdf.

9) It was fun assignment to create/implement the data structures. It was cool to actually see meaningful results at the end of the day! It was very interesting trying to work with relatively large amounts of data using our own structures.
