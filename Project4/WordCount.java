/*
 *
 *	File: WordCount.java
 *
 *	Authors: Ankit Gandhi <csghandiankit@gmail.com>
 *	         John Moon <johncarlmoon@gmail.com>
 *
 *	Purpose: Performs word frequency analysis on the input file.
 *
 */

import java.io.IOException;
import java.util.Arrays;

public class WordCount
{
	static boolean insort = true;
	static boolean qsort = false;
	static boolean msort = false;

    private static void countWords(String file, DataCounter counter)
	{
		/* Process the file and add the data into the table or tree */
        try {
            FileWordReader reader = new FileWordReader(file);
            String word = reader.nextWord();
            while (word != null) {
                counter.incCount(word);
                word = reader.nextWord(); }
        } catch (IOException e) {
            System.err.println("Error processing " + file + e);
            System.exit(1);
        }

		/* Create an array of counts, then sort it */
        DataCount<String>[] counts = counter.getCounts();
		if (insort)
			insort(counts);
		else if (qsort)
			quicksort(counts, 0, counts.length - 1);
		else if (msort)
			mergesort(counts);
		else
			insort(counts);

		sortByAlpha(counts);

		/* Print the array after it's sorted */
        for (DataCount<String> c : counts)
            System.out.println(c.count + "  \t" + c.data);
    }

	/* Sort the count array in descending order using the insertion sort
	   algorithm */
    private static void insort(DataCount<String>[] counts)
	{
		for (int i = 1; i < counts.length; i++) {
			DataCount<String> x = counts[i];
			int j;
			for (j = i - 1; j >= 0; j--) {
				if (counts[j].count >= x.count) {
					break;
				}
				counts[j+1] = counts[j];
			}
			counts[j+1] = x;
		}
    }

	/* Sort the count array in descending order using the Quicksort algorithm */
    private static void quicksort(DataCount<String>[] counts,
								  int lowIndex, int highIndex)
	{
		int i = lowIndex;
		int j = highIndex;
		int pivot =  counts[lowIndex + (highIndex - lowIndex) / 2].count; // Pivot is the middle of the array

		while (i <= j) {
			while (counts[i].count > pivot) {
				if (i <= j)
					i++;
				else
					break;
			}
			while (counts[j].count < pivot) {
				if (j >= i)
					j--;
				else
					break;
			}

			if (i <= j) {
				DataCount<String> tmp = counts[i];
				counts[i] = counts[j];
				counts[j] = tmp;
				i++;
				j--;
			}
		}

		/* Recursively divide into subarrays */
		if (lowIndex < j)
			quicksort(counts, lowIndex, j);
		if (i < highIndex)
			quicksort(counts, i, highIndex);
    }

	/**
	* Internal method that makes recursive calls.
	* @param a an array of Comparable items.
	* @param tmpArray an array to place the merged result.
	* @param left the left-most index of the subarray.
	* @param right the right-most index of the subarray.
	*/
	private static void mergesort(DataCount<String>[] a, DataCount<String>[] tmpArray,
					int left, int right) {
		if (left < right) {
			int middle = (left + right) / 2;
			mergesort(a, tmpArray, left, middle);
			mergesort(a, tmpArray, middle + 1, right);
			merge(a, tmpArray, left, middle + 1, right);
		}
	}

	/**
	* Mergesort algorithm.
	* @param a an array of Comparable items.
	*/
	public static void mergesort(DataCount<String>[] a) {
		DataCount[] tmpArray = new DataCount[a.length];
		mergesort(a, tmpArray, 0, a.length - 1);
	}

	/**
	* Internal method that merges two sorted halves of a subarray.
	* @param a an array of Comparable items.
	* @param tmpArray an array to place the merged result.
	* @param leftPos the left-most index of the subarray.
	* @param rightPos the index of the start of the second half.
	* @param rightEnd the right-most index of the subarray.
	*/
	private static void merge(DataCount<String>[] a, DataCount<String>[] tmpArray,
			int leftPos, int rightPos, int rightEnd) {
		int leftEnd = rightPos - 1;
		int tmpPos = leftPos;
		int numElements = rightEnd - leftPos + 1;

		// Main loop
		while (leftPos <= leftEnd && rightPos <= rightEnd)
			if (a[leftPos].count > a[rightPos].count)
				tmpArray[tmpPos++] = a[leftPos++];
			else
				tmpArray[tmpPos++] = a[rightPos++];

		while (leftPos <= leftEnd) // Copy rest of first half
			tmpArray[tmpPos++] = a[leftPos++];

		while (rightPos <= rightEnd) // Copy rest of right half
			tmpArray[tmpPos++] = a[rightPos++];

		// Copy tmpArray back
		for (int i = 0; i < numElements; i++, rightEnd--)
			a[rightEnd] = tmpArray[rightEnd];
	}

	/* Sort array alphabetically as a secondary sort */
	private static void sortByAlpha(DataCount<String>[] counts)
	{
		int arrSize = counts.length;
		int i = 0;
		while (i < arrSize) {
			int tmpCount = counts[i].count;
			int subArrLength;
			if (tmpCount > 1) {
				subArrLength = 1;
				for (int j = i + 1; counts[j].count == tmpCount; j++) {
					subArrLength++;
				}
			} else {
				subArrLength = arrSize - i;
			}

			if (subArrLength > 1)  {
				DataCount[] subArr = new DataCount[subArrLength];
				for (int x = 0; x < subArrLength; x++) {
					subArr[x] = counts[i + x];
				}

				Arrays.sort(subArr, new StringComparator());

				for (int x = 0; x < subArrLength; x++) {
					counts[i + x] = subArr[x];
				}
			}

			i = i + subArrLength;
		}
	}

	public static void main(String[] args)
	{
        if (args.length != 3) {
            System.err.println("Error - Usage... \"WordCount [-a | -b | -h] [-is | -qs | -ms] <filename...>\"");
            System.exit(1);
        } else {

			if (args[1].compareTo("-is") == 0) {
				insort = true;
				qsort = false;
				msort = false;
			} else if (args[1].compareTo("-qs") == 0) {
				insort = false;
				qsort = true;
				msort = false;
			} else if (args[1].compareTo("-ms") == 0) {
				insort = false;
				qsort = false;
				msort = true;
			} else {
				System.err.println("Error - Usage... \"WordCount [-a | -b | -h] [-is | -qs | -ms] <filename...>\"");
			}

			/* If we get the "-a" flag, use an AVL tree
			If we get the "-h" flag, use a hash table
			If we get the "-b" flag, use a BST */
			DataCounter counter = null;
			if (args[0].compareTo("-a") != 0)
				counter = new AVLTree();
			else if (args[0].compareTo("-h") != 0)
				counter = new HashCounter();
			else if (args[0].compareTo("-b") != 0)
				counter = new BinarySearchTree();
			else {
				System.err.println("Error - Usage... \"WordCount [-a | -b | -h] [-is | -qs | -ms] <filename...>\"");
				System.exit(1);
			}

			countWords(args[2], counter);
		}
        System.exit(1);
    }
}
