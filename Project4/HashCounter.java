/*
 *
 *	File: HashCounter.java
 *
 *	Authors: Ankit Gandhi <csghandiankit@gmail.com>
 *	         John Moon <johncarlmoon@gmail.com>
 *
 *	Purpose: Implements the DataCounter using a string-based hash table
 *
 */

class HashCounter implements DataCounter<String>
{
	/* Creates hash table with large prime number */
	HashTable hashTable = new HashTable(4999);

	/* Increments the qty of given data. If data does not exist, creates data. */
	public void incCount(String data)
	{
		hashTable.incCount(data);	
	}	

	/* Returns the number of data-holding elements in the hash table */
	public int getSize()
	{
		int sum = 0;
		for (int i = 0; i < hashTable.getCap(); i++) {
			if (hashTable.table[i].getCell() != null) {
				ListNode traverse = hashTable.table[i];
				while (traverse.getNext() != null) {
					sum++;
					traverse = traverse.getNext();
				}
				sum++;
			}
		}
		return sum;
	}

	/* Creates an array of counters - fills with data from hash table */
	public DataCount<String>[] getCounts()
	{
		int size = getSize();
		DataCount[] arr = new DataCount[size];
		int j = 0;
		for (int i = 0; i < hashTable.getCap(); i++) {
			ListNode traverse = hashTable.table[i];
			if (traverse.getCell() != null) {
				while (traverse.getNext() != null) {
					arr[j] = traverse.getCell();
					j++;
					traverse = traverse.getNext();
				}
				arr[j] = traverse.getCell();
				j++;
			}
		}
		return arr;
	}
}
