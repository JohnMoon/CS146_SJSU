/*
 *
 *	File: BinarySearchTree.java
 *
 *	Authors: Ankit Gandhi <csghandiankit@gmail.com>
 *	         John Moon <johncarlmoon@gmail.com>
 *
 *	Purpose: Implements the DataCounter interface using a binary search tree to
 *			 store the data items and counts.
 *
 */

public class BinarySearchTree<E extends Comparable<? super E>> implements
        DataCounter<E> 
{

	/* The root of the binary search tree. root is null if and only if the tree is empty */
    protected BSTNode overallRoot;

    /* Number of nodes in the binary search tree */
    protected int size;

    /**
     * Inner (non-static) class to represent a node in the tree. Each node
     * includes a String and an integer count. The class is protected so that it
     * may be accessed by subclasses of BSTCounter.
     */
    protected class BSTNode 
	{ 
		public BSTNode left;
        public BSTNode right;
        public E data;
        public int count;
		public int height;

        public BSTNode(E data) {
            this.data = data;
            count = 1;
            left = right = null;
            size++;
        }
    }

    public BinarySearchTree() 
	{
        overallRoot = null;
        size = 0;
    }

    public void incCount(E data) 
	{
        if (overallRoot == null) {
            overallRoot = new BSTNode(data);
        } else {
            // traverse the tree
            BSTNode currentNode = overallRoot;
            while (true) {

                // compare the data to be inserted with the data at the current
                // node
                int cmp = data.compareTo(currentNode.data);

                if (cmp == 0) {
                    // current node is a match
                    currentNode.count++;
                    return;
                } else if (cmp < 0) {
                    // new data goes to the left of the current node
                    if (currentNode.left == null) {
                        currentNode.left = new BSTNode(data);
                        return;
                    }
                    currentNode = currentNode.left;
                } else {
                    // new data goes to the right of the current node
                    if (currentNode.right == null) {
                        currentNode.right = new BSTNode(data);
                        return;
                    }
                    currentNode = currentNode.right;
                }
            }
        }
    }

    public int getSize() 
	{
        return size;
    }

    public DataCount<E>[] getCounts() 
	{
    	@SuppressWarnings("unchecked")
        DataCount<E>[] counts = new DataCount[size];
        if (overallRoot != null)
            traverse(overallRoot, counts, 0);
        return counts;
    }

    /**
     * Do an inorder traversal of the tree, filling in an array of DataCount
     * objects with the count of each element. Doing an inorder traversal
     * guarantees that the result will be sorted by element. We fill in some
     * contiguous block of array elements, starting at index, and return the
     * next available index in the array.
     *
     * @param counts The array to populate.
     */
    protected int traverse(BSTNode root, DataCount<E>[] counts, int idx) 
	{
        if(root != null) {
            idx = traverse(root.left, counts, idx);
            counts[idx] = new DataCount<E>(root.data, root.count);
            idx = traverse(root.right, counts, idx + 1);
        }
        return idx;
    }

    /**
     * Dump the contents of the tree to a String (provided for debugging and
     * unit testing purposes).
     *
     * @return a textual representation of the tree.
     */
    protected String dump() 
	{
        if (overallRoot != null)
            return dump(overallRoot);
        return "<empty tree>";
    }

    /**
     * Dump the contents of the subtree rooted at this node to a String
     * (provided for debugging purposes).
     *
     * @return a textual representation of the subtree rooted at this node.
     */
    protected String dump(BSTNode root) 
	{
        if(root == null)
            return ".";

        String out = "([" + root.data + "," + root.count + "] ";
        out += dump(root.left);
        out += " ";
        out += dump(root.right);
        out += ")";

        return out;
    }
   
	/* Return the height of the tree */	
    protected int height(BSTNode root) 
    {
        if(root == null) 
            return -1;
        else 
            return Math.max(height(root.left), height(root.right)) + 1;
    }
}
